from datetime import datetime
from enum import Enum
from pydantic import Field, BaseModel
from typing import List
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage

class ProductMedia (BaseModel):
    filename: str = None
    contentType: str = None

class ProductBase (DefaultData):
    title: str = None
    subtitle: str = None
    tipe: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    listDetail: List[ListDetail] = []
    image: List[ProductMedia] = []
    submitdate: datetime = None

class ProductOnDB (ProductBase):
    id: ObjectIdStr = Field(alias="_id")

class ProductPage (DefaultPage):
    content: List[ProductOnDB] = []