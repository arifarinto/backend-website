from pydantic import Field
from typing import List

from pydantic.main import BaseModel
from model.util import DefaultData, ObjectIdStr, DefaultPage

class ContactBase(BaseModel):
    phone: str = None
    address: str = None
    email: str = None
    instagram: str = None
    facebook: str = None
    youtube: str = None
    whatsapp: str = None
    linkedin: str = None
    twitter: str = None

class HomeBase(DefaultData):
    webName: str = None
    webDescription: str = None
    webImageIcon: str = None
    contact: ContactBase = None
    mainColor: str = None
    secondaryColor: str = None
    headTitle: str = None
    headSubtitle: str = None
    headImage: str = None
    headUrl: str = None
    videoUrl: str = None
    ppdbLink: str = None
    newsTitle: str = None
    newsSubtitle: str = None
    testimonyTitle: str = None
    testimonySubtitle: str = None
    agendaTitle: str = None
    agendaSubTitle: str = None
    bottomTitle: str = None
    bottomSubtitle: str = None
    jmlSiswaTk : int =None
    jmlSiswaSD : int = None
    jmlSiswaSmp : int = None
    jmlSiswaSma : int =None

class HomeOnDB(HomeBase):
    id: ObjectIdStr = Field(alias="_id")

class HomePage (DefaultPage):
    content: List[HomeOnDB] = []