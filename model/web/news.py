from datetime import datetime
from email.mime import image
from enum import Enum
from pydantic import Field, BaseModel
from typing import List
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage

class NewsMedia (BaseModel):
    filename: str = None
    contentType: str = None

class NewsBase (DefaultData):
    title: str = None
    subtitle: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    description2: str = None
    description3: str = None
    description4: str = None
    note: str = None
    viewCount: int = 0
    url : str = None
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    listDetail: List[ListDetail] = []
    submitdate: datetime = None
    image: List[NewsMedia] = []
    

class NewsOnDB (NewsBase):
    id: ObjectIdStr = Field(alias="_id")

class NewsPage (DefaultPage):
    content: List[NewsOnDB] = []

#---------------------------------------------------------------------------------

class NewsBaseOut (DefaultData):
    title: str = None
    description: str = None
    description2: str = None
    description3: str = None
    description4: str = None
    submitdate: str = None
    image: List[NewsMedia] = []
    

class NewsOnDBOut (NewsBaseOut):
    id: ObjectIdStr = Field(alias="_id")

class NewsPageOut (DefaultPage):
    content: List[NewsOnDBOut] = []