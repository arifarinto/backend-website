from datetime import date, datetime
from enum import Enum
from pydantic import Field
from typing import List

from pydantic.main import BaseModel
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage
from util.util_waktu import dateNow, dateTimeNow

# class GenEnum (str, Enum):
#     Pria = "Pria"
#     Wanita = "Wanita"

# class LastEducateEnum (str, Enum):
#     SMA_SMK = "SMA/SMK"
#     Diploma = "Diploma"
#     S1 = "S1"
#     S2 = "S2"
#     S3 = "S3"

class StatusEnum (str, Enum):
    Full_time = "Full-time"
    Part_Time = "Part-Time"
    Freelance = "Freelance"

# class PelamarData (BaseModel):
#     idPelamar : str = None
#     firstName : str = None
#     lastName : str = None
#     email : str = None
#     nohp : str = None
#     tglLahir : date = None
#     gender : GenEnum = None
#     address : str = None
#     lastEducation : LastEducateEnum = None
#     startDate : date = None
#     website : str = None #optional
#     URL_linkedin : str = None #optional
#     cv : str = None
#     tglApply : datetime = None

class KarirBase (DefaultData):
    position: str = None
    deadline: date = None
    status: StatusEnum = None
    enable: bool = True
    tags: List[str] = []
    requirement : List[str] = []
    jobdesk : List[str] = []
    benefit : List[str] = []
    note: str = None
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    # pelamar : List[PelamarData] = []
    submitdate: datetime = None

    class Config:
        schema_extra = {
            "example": {
                "position": "string",
                "deadline": "1945-8-17",
                "status": "Full-time",
                "enable" : "true",
                "tags": [],
                "requirement": [
                    "1. string",
                    "2. string",
                    "3. string"
                ],
                "jobdesk": [
                    "1. string",
                    "2. string",
                    "3. string"
                ],
                "benefit": [
                    "1. string",
                    "2. string",
                    "3. string"
                ],
                "note": "string",
                "iconUrl": "string",
                "imageUrl": "string",
                "submitdate": "2022-02-22T08:35:58.318Z"
            }
        }

class KarirOnDB (KarirBase):
    id: ObjectIdStr = Field(alias="_id")    

class KarirPage (DefaultPage):
    content: List[KarirOnDB] = []