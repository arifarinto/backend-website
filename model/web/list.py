from datetime import datetime
from enum import Enum
from pydantic import BaseModel
from model.util import DefaultData, ObjectIdStr

class menuEnum(str, Enum):
    about = "about"
    achievement = "achievement"
    feature = "feature"
    gallery = "gallery"
    partner = "partner"
    pricing = "pricing"
    product = "product"
    video_product = "video product"
    news = "news"
    tutorial = "tutorial"

class ListDetail(BaseModel):
    listId: str = None
    createTime: datetime = None
    title: str = None
    subTitle: str = None
    text1: str = None
    text2: str = None
    text3: str = None
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    url1: str = None
    url2: str = None