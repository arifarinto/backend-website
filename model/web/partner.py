from pydantic import Field
from typing import List
from model.web.list import ListDetail
from model.util import DefaultData, ObjectIdStr, DefaultPage

class PartnerBase (DefaultData):
    title: str = None
    subtitle: str = None
    enable: bool = True
    tags: List[str] = []
    description: str = None
    note: str = None
    viewCount: int = 0
    iconUrl: str = None #nama file icon nya
    imageUrl: str = None #nama file image nya
    listDetail: List[ListDetail] = []

class PartnerOnDB (PartnerBase):
    id: ObjectIdStr = Field(alias="_id")

class PartnerPage (DefaultPage):
    content: List[PartnerOnDB] = []