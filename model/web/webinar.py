from datetime import date, datetime, time
from enum import Enum
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage

class PesertaWebinar (BaseModel):
    idPeserta: str = None
    nama : str = None
    usia : str = None
    email : str = None
    whatsapp : str = None
    jabatan : str = None
    institusi : str = None
    jenisInstitusi : str = None
    provinsi : str = None
    kota : str = None
    instagram : str = None
    facebook : str = None
    websites : str = None
    mengetahui : List[str] = None
    pertanyaanProduk: str =  None
    produkTki : List[str] = None
    tglsubmit : datetime = None

class WebinarMedia (BaseModel):
    filename: str = None
    contentType: str = None

#-------------------------------------------------------------------------------------------

class WebinarBase (DefaultData):
    judul : str = None
    description: str = None
    tanggal : date = None
    jam : time = None
    waktu: datetime = None
    host : str = None
    photoHost : str = None
    urlWebinar : str = None
    urlYoutube : str = None
    urlGroup : str = None
    enable: bool = True
    tags: List[str] = []
    note: str = None
    viewCount: int = 0
    iconUrl: str = None
    peserta : List[PesertaWebinar] = []
    image: List[WebinarMedia] = []

    class Config:
        schema_extra = {
            "example": {
                "judul": "sasdfa",
                "description": "asdfasd",
                "tanggal": "1945-8-17",
                "jam" : "13:00",
                # "host" : "string",
                # "photoHost" : "string",
                "urlWebinar" : "asdfads",
                "urlYoutube" : "asdfads",
                "urlGroup" : "asdfads",
                # "enable" : True,
                # "tags": [],
                # "note": "string",
                # "viewCount": 0,
                # "iconUrl": "string"
            }
        }

class WebinarOnDB (WebinarBase):
    id: ObjectIdStr = Field(alias="_id")

class WebinarPage (DefaultPage):
    content: List[WebinarOnDB] = []

#-------------------------------------------------------------------------------------------

class WebinarBaseOut (DefaultData):
    judul : str = None
    description: str = None
    jam : time = None
    waktu: str = None
    urlWebinar : str = None
    urlYoutube : str = None
    urlGroup : str = None

class WebinarOnDBOut (WebinarBaseOut):
    id: ObjectIdStr = Field(alias="_id")

class WebinarPageOut (DefaultPage):
    content: List[WebinarOnDBOut] = []

#-------------------------------------------------------------------------------------------

class WebinarPesertaList (BaseModel):
    id: ObjectIdStr =  Field(alias="_id")
    judul: str = None
    total: int = None
    tanggal: str = None
    peserta: List[PesertaWebinar] = []

class pageWebinarPesertaList(BaseModel):
    content: List[WebinarPesertaList]= []

class pageReportPesertaOnDB(BaseModel):
    id: ObjectIdStr = Field(alias="_id")
    judul:str = None
    description:str = None
    tanggal:str = None
    peserta:PesertaWebinar = {}

class pageReportPeserta(BaseModel):
    content: List[pageReportPesertaOnDB] = []


# Tambahan
class PesertaWebinarOut (BaseModel):
    idPeserta: str = None
    nama : str = None
    usia : str = None
    email : str = None
    whatsapp : str = None
    jabatan : str = None
    institusi : str = None
    jenisInstitusi : str = None
    provinsi : str = None
    kota : str = None
    instagram : str = None
    facebook : str = None
    websites : str = None
    mengetahui : List[str] = None
    pertanyaanProduk: str =  None
    produkTki : List[str] = None
    tglsubmit : str = None

class pageReportPesertaOutOnDB(BaseModel):
    id: ObjectIdStr = Field(alias="_id")
    judul:str = None
    description:str = None
    waktu:datetime = None
    peserta:PesertaWebinarOut = {}

class pageReportPesertaOut(BaseModel):
    content: List[pageReportPesertaOutOnDB] = []