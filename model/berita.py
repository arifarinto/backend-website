from datetime import date, datetime
from util.util_waktu import dateTimeNow
from pydantic import BaseModel, Field
from typing import List
from model.util import DefaultData, ObjectIdStr, DefaultPage
from model.default import AddressData, IdentityData, CredentialData, UserInput
from util.util import RandomNumber, RandomString
from faker import Faker

# [PERTANYAAN]
# Entitas berita ini kayaknya persis sama dengan entitas info
# kenapa nggak jadi satu aja?


class BeritaBase(DefaultData):
    title: str = None
    subTitle: str = None
    isPublish: bool = None
    isHeadLine: bool = None
    tags: List[str] = []
    description: str = None
    viewCount: int = None
    urlFb: str = None
    urlIg: str = None
    urlYoutube: str = None
    urlDownload: str = None
    image: str = None


class BeritaOnDB(BeritaBase):
    id: ObjectIdStr = Field(alias="_id")


class BeritaPage(DefaultPage):
    content: List[BeritaOnDB] = []
