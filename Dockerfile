FROM ubuntu:focal-20211006
RUN apt update
RUN apt install -y nano python3-pip uvicorn curl net-tools
COPY . /app
RUN pip3 install -r /app/requirements2.txt
WORKDIR /app
RUN chmod +x /app/start.sh
CMD /app/start.sh