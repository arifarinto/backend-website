from fastapi.exceptions import HTTPException
from util.util import ValidateObjectId
from model.default import NotifBase, NotifData, NotifMembershipBase
from config.config import MGDB
from util.util_waktu import dateTimeNow
from bson.objectid import ObjectId

dbase = MGDB.tbl_notif


async def GetNotifOr404(id: str):
    _id = ValidateObjectId(id)
    notif = await dbase.find_one({"_id": _id})
    if notif:
        return notif
    else:
        raise HTTPException(status_code=404, detail="Notif not found")


async def CreateNotif(
    isUserExist: bool, userId: ObjectId, companyId: ObjectId, notif: NotifData
):
    notif.createTime = dateTimeNow()
    notif.id = ObjectId()
    notif.userId = userId

    if isUserExist == True:
        print("user is exist, append notif")
        await dbase.update_one(
            {"userId": userId}, {"$addToSet": {"notif": notif.dict()}}
        )
    else:
        cekNotif = await dbase.find_one({"userId": userId})
        if cekNotif:
            print("append notif")
            await dbase.update_one(
                {"userId": userId}, {"$addToSet": {"notif": notif.dict()}}
            )
        else:
            print("insert notif")
            dnotif = NotifBase()
            dnotif.createTime = dateTimeNow()
            dnotif.updateTime = dateTimeNow()
            dnotif.userId = userId
            dnotif.companyId = companyId
            dnotif.notif.append(notif.dict())
            await dbase.insert_one(dnotif.dict())
    return notif


async def CreateNotifMembership(userId: str, notif: NotifData):
    print("create notif called")
    dbase = MGDB["tbl_notif_membership"]
    notifInDb = await dbase.find_one({"userId": ObjectId(userId)})
    if notifInDb:
        print("notif ada")
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$addToSet": {"notif": notif.dict()}}
        )
    else:
        notifBase = NotifMembershipBase()
        notifBase.userId = ObjectId(userId)
        notifBase.notif.append(notif.dict())
        await dbase.insert_one(notifBase.dict())
