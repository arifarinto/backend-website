#!/bin/bash
cd /app
#/usr/sbin/nginx &
uvicorn api_web:app --reload --port 80 --host 0.0.0.0 
# Wait for any process to exit
#wait -n
# Exit with status of process that exited first
#exit $?
