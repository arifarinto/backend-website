
from fastapi import FastAPI
from elasticapm.contrib.starlette import make_apm_client, ElasticAPM
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from pydantic.networks import import_email_validator
from config import config
from route.auth import router_auth

from route.web.about import router_about
from route.web.achievement import router_achievement
from route.web.faq import router_faq
from route.web.feature import router_feature
from route.web.gallery import router_gallery
from route.web.home import router_home
from route.web.karir import router_karir
from route.web.news import router_news
from route.web.partner import router_partner
from route.web.pricing import router_pricing
from route.web.product import router_product
from route.web.slider import router_slider
from route.web.testimony import router_testimony
from route.web.tutorial import router_tutorial
from route.web.video_product import router_video_product
from route.web.webinar import router_webinar
from route.web.weekly import router_weekly
from route.web.crud_list_detail import router_listdetail
from route.web.hero import router_hero

app = FastAPI(openapi_url="/web_api/openapi.json",docs_url="/web_api/swgr")
app.mount("/web_api/static", StaticFiles(directory="static"), name="static")
if config.ENVIRONMENT == 'production':
    #setup Elastic APM Agent
    apm = make_apm_client({
        'SERVICE_NAME': 'TKI-backend-web',
        'SERVER_URL': 'http://apm-server.logging:8200',
        'ELASTIC_APM_TRANSACTION_IGNORE_URLS': ['/health'],
        'METRICS_SETS': "elasticapm.metrics.sets.transactions.TransactionsMetricSet",
    })
    app.add_middleware(ElasticAPM, client=apm)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*","*"],
    allow_credentials=True,
    allow_methods=["*","*"],
    allow_headers=["*","*"],
)

@app.get("/health")
async def health():
    return {"status": "ok"}


app.include_router(router_auth,prefix="/web_api/auth",tags=["auth"],responses={404: {"description": "Not found"}})
app.include_router(router_about,prefix="/web_api/web_about",tags=["about"],responses={404: {"description": "Not found"}})
app.include_router(router_achievement,prefix="/web_api/web_achievement",tags=["achievement"],responses={404: {"description": "Not found"}})
app.include_router(router_faq,prefix="/web_api/web_faq",tags=["faq"],responses={404: {"description": "Not found"}})
app.include_router(router_feature,prefix="/web_api/web_feature",tags=["feature"],responses={404: {"description": "Not found"}})
app.include_router(router_gallery,prefix="/web_api/web_gallery",tags=["gallery"],responses={404: {"description": "Not found"}})
app.include_router(router_home,prefix="/web_api/web_home",tags=["home"],responses={404: {"description": "Not found"}})
app.include_router(router_karir,prefix="/web_api/web_karir",tags=["karir"],responses={404: {"description": "Not found"}})
app.include_router(router_news,prefix="/web_api/web_news",tags=["news"],responses={404: {"description": "Not found"}})
app.include_router(router_partner,prefix="/web_api/web_partner",tags=["partner"],responses={404: {"description": "Not found"}})
app.include_router(router_pricing,prefix="/web_api/web_pricing",tags=["pricing"],responses={404: {"description": "Not found"}})
app.include_router(router_product,prefix="/web_api/web_product",tags=["product"],responses={404: {"description": "Not found"}})
app.include_router(router_slider,prefix="/web_api/web_slider",tags=["slider"],responses={404: {"description": "Not found"}})
app.include_router(router_testimony,prefix="/web_api/web_testimony",tags=["testimony"],responses={404: {"description": "Not found"}})
app.include_router(router_tutorial,prefix="/web_api/web_tutorial",tags=["tutorial"],responses={404: {"description": "Not found"}})
app.include_router(router_video_product,prefix="/web_api/web_video_product",tags=["video product"],responses={404: {"description": "Not found"}})
app.include_router(router_webinar,prefix="/web_api/web_webinar",tags=["webinar"],responses={404: {"description": "Not found"}})
app.include_router(router_weekly,prefix="/web_api/web_weekly",tags=["weekly"],responses={404: {"description": "Not found"}})
app.include_router(router_listdetail,prefix="/web_api/web_listdetail",tags=["list detail"],responses={404: {"description": "Not found"}})
app.include_router(router_hero,prefix="/web_api/web_hero",tags=["hero"],responses={404: {"description": "Not found"}})


@app.on_event("startup")
async def app_startup():
    config.load_config()

@app.on_event("shutdown")
async def app_shutdown():
    config.close_db_client()