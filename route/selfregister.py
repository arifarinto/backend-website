from bson.objectid import ObjectId
from config.config import MGDB, ENVIRONMENT
from fastapi import APIRouter, Depends, HTTPException, Security, Response, BackgroundTasks
from datetime import datetime, timedelta

from model.default import IdentityData, RegisterBase, JwtToken, UserCredential
from function.user import CreateUser, CheckEmailWithCompany
from model.util import EmailData
from route.auth import create_access_token, get_current_user, create_token_by_username_password, ACCESS_TOKEN_EXPIRE_MINUTES
from function.company import GetCompanyOr404
from util.util import ValidateEmail, ValidateObjectId, RandomNumber, ValidPhoneNumber, RandomString, ValidPassword
from util.util_waktu import dateNowStr, dateTimeNow

router_selfregister = APIRouter()


@router_selfregister.post("/input_register", response_model=dict)
async def input_register(input: RegisterBase, backgroundTask: BackgroundTasks):
    if input.tblName == "user_admin":
        raise HTTPException(status_code=400, detail="Inputan tidak sesuai, silahkan ganti tabel name")

    # cek apakah nohp sudah terdaftar
    if input.email:
        if not ValidateEmail(input.email):
            raise HTTPException(status_code=400, detail="Email tidak valid")
        if await CheckEmailWithCompany(input.tblName, input.email, input.companyId) == True:
            raise HTTPException(status_code=400, detail="Email sudah terdaftar di company yang sama, gunakan email yang lain atau silahkan login")
    # kirim email dan link OTP
    data = RegisterBase()
    data.email = input.email.lower().strip()
    data.phone = input.phone.strip()
    data.companyId = input.companyId
    data.createTime = dateTimeNow()
    data.expiredTime = dateTimeNow() + timedelta(minutes=5)
    data.otpCode = RandomNumber(4)
    data.otpStatus = False
    data.tblName = input.tblName

    # kirim email
    dcomp = await GetCompanyOr404(input.companyId)
    emailBase = EmailData()
    emailBase.companyName = dcomp['name']
    emailBase.emailTo = data.email
    emailBase.token = data.otpCode
    # await SendOtpRegisterEmail(emailBase)
    # backgroundTask.add_task(SendOtpRegisterEmail, emailBase)

    #simpan ke notif

    reg_op = await MGDB.tbl_register_verify.insert_one(data.dict())
    if reg_op.inserted_id:
        if ENVIRONMENT == 'development':
            return {"regId": str(reg_op.inserted_id), "otp": data.otpCode}
        else:
            return {"regId": str(reg_op.inserted_id), "otp": ""}


@router_selfregister.post("/validation_by_otp", response_model=dict)
async def validation_by_otp(regId: str, otp: str):
    # register by admin gak perlu kesini
    otp = otp.strip()
    _id = ValidateObjectId(regId)
    cekOtp = await MGDB.tbl_register_verify.find_one({"_id": _id, "otpStatus": False})
    if not cekOtp:
        raise HTTPException(status_code=404, detail="Data Not Found")
    if cekOtp['otpCode'] == otp:
        tempPwd = RandomString(10)
        await MGDB.tbl_register_verify.update_one(
            {"_id": _id},
            {"$set": {"password": tempPwd, "otpStatus": True}}
        )
        return {"regId": regId, "tempPwd": tempPwd, "email": cekOtp['email']}
    else:
        raise HTTPException(status_code=400, detail="OTP not valid")


@router_selfregister.post("/create_account_and_password", response_model=dict)
async def create_account_and_password(regId: str, tempPwd: str, name: str, pwd: str, response: Response):
    _id = ValidateObjectId(regId)
    pwd = pwd.strip()
    cek = await MGDB.tbl_register_verify.find_one({"_id": _id, "otpStatus": True})
    if not cek:
        raise HTTPException(status_code=404, detail="Data Registrasi tidak ditemukan")
    ValidPassword(pwd)
    if cek['password'] == tempPwd:
        # create user return token
        user = UserCredential()
        user.companyId = ObjectId(cek['companyId'])
        user.name = name.strip().upper()
        user.phone = cek['phone']
        user.email = cek['email']
        arName = user.name.split(' ')
        user.username = arName[0] + RandomNumber(4)
        identity = IdentityData()
        identity.dateOfBirth = dateNowStr()
        user.identity = identity
        user_op = await CreateUser(cek['tblName'], user, pwd, False, ['user'])

        # update status
        await MGDB.tbl_register_verify.update_one(
            {"_id": _id},
            {"$set": {"otpStatus": False}}
        )

        data_token = await create_token_by_username_password(cek['tblName'], str(user_op.userId), pwd, cek['companyId'])

        access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
        access_token = create_access_token(data_token, expires_delta=access_token_expires)
        
        btoken = "Bearer " + str(access_token)
        # tambah header
        response.headers["Authorization"] = btoken
        return {
            "access_token": access_token,
            "userId": data_token.userId,
            "companyId": data_token.companyId,
            "active": True,
            "isAdmin": data_token.isAdmin,
            "role": data_token.roles,
            "name": user.name,
            "image": user.profileImage,
            "email": user.email,
            "phone": user.phone,
            "isWa": user.isWa,
            "dateOfBirth": user.identity.dateOfBirth,
            "isFirstLogin": user_op.credential.isFirstLogin
        }

    else:
        raise HTTPException(status_code=404, detail="Temporary Password tidak valid")
