# backend/tancho/tutorials/routes.py
import os
import shutil
from bson.objectid import ObjectId
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import math
from pprint import pprint
from fastapi.responses import FileResponse

from model.web.tutorial import TutorialBase, TutorialMedia, TutorialOnDB, TutorialPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_tutorial = APIRouter()
dbase = MGDB.website_tutorial

async def GetTutorialOr404(id: str):
    _id = ValidateObjectId(id)
    tutorial = await dbase.find_one({"_id": _id})
    if tutorial:
        return tutorial
    else:
        raise HTTPException(status_code=404, detail="Tutorial not found")
# =================================================================================
#UPLOAD UPDATE SHOW IMAGE

#UPLOAD IMAGE
@router_tutorial.post("/tutorial/upload_gambar")
async def upload_gambar(
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "tutorial"
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)

#EDIT IMAGE
@router_tutorial.post("/tutorial/upload_gambar/edit/{id}")
async def edit_gambar(
    id: str,
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "tutorial"

    pipeline = [
        {"$match": {"_id": ObjectId(id)}},
        {"$unwind": {"path": "$image"}},
        {"$project": {
            "image": 1
        }}
    ]
    file = await dbase.aggregate(pipeline).to_list(1000)
    filename = file[0]['image']['filename']
    pprint(filename)
    img = os.path.join(path, path2, filename)
    if os.path.exists(img):
        os.remove(img)

    data_in = TutorialMedia(
        filename=gambar.filename,
        contentType=gambar.content_type
    )
    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$pull": {"image": {"filename": filename}}},
    )

    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$addToSet": {"image": data_in.dict()}},
    )
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)


#GET IMAGE
@router_tutorial.get("/tutorial/get_gambar/{filename}")
async def get_upload_gambar(
    filename: str
):
    path = "/app/upload-image"
    path2 = "tutorial"
    file_path = os.path.join(path, path2, filename)
    return FileResponse(file_path)
# =================================================================================

@router_tutorial.post("/tutorial", response_model=TutorialOnDB)
async def add_tutorial(data_in: TutorialBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    tutorial = IsiDefault(data_in, current_user)
    tutorial.tags = ListToUp(tutorial.tags)
    tutorial_op = await dbase.insert_one(tutorial.dict())
    if tutorial_op.inserted_id:
        tutorial = await GetTutorialOr404(tutorial_op.inserted_id)
        return tutorial


@router_tutorial.post("/get_tutorial/{companyId}", response_model=dict)
async def get_all_tutorial(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = TutorialPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_tutorial.get("/tutorial/{id}", response_model=TutorialOnDB)
async def get_tutorial_by_id(id: ObjectId = Depends(ValidateObjectId)):
    tutorial = await dbase.find_one({"_id": id})
    if tutorial:
        return tutorial
    else:
        raise HTTPException(status_code=404, detail="Tutorial not found")


@router_tutorial.delete("/tutorial/{id}", dependencies=[Depends(GetTutorialOr404)], response_model=dict)
async def delete_tutorial_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    tutorial_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if tutorial_op.deleted_count:
        return {"status": f"deleted count: {tutorial_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_tutorial.put("/tutorial/{id_}", response_model=TutorialOnDB)
async def update_tutorial(id_: str, data_in: TutorialBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    tutorial = IsiDefault(data_in, current_user, True)
    tutorial.updateTime = dateTimeNow()
    if tutorial.tags : tutorial.tags = ListToUp(tutorial.tags)
    tutorial_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": tutorial.dict(skip_defaults=True)}
    )
    if tutorial_op.modified_count:
        return await GetTutorialOr404(id_)
    else:
        raise HTTPException(status_code=304)