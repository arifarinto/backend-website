from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.hero import HeroBase, HeroOnDB, HeroPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_hero = APIRouter()
dbase = MGDB.website_hero

async def GetHeroOr404(id: str):
    _id = ValidateObjectId(id)
    hero = await dbase.find_one({"_id": _id})
    if hero:
        return hero
    else:
        raise HTTPException(status_code=404, detail="hero not found")

# =================================================================================

@router_hero.post("/hero", response_model=HeroOnDB)
async def add_hero(data_in: HeroBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    hero = IsiDefault(data_in, current_user)
    hero.tags = ListToUp(hero.tags)
    hero_op = await dbase.insert_one(hero.dict())
    if hero_op.inserted_id:
        hero = await GetHeroOr404(hero_op.inserted_id)
        return hero


@router_hero.post("/get_hero/{companyId}", response_model=dict)
async def get_all_heros(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = HeroPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_hero.get("/hero/{id}", response_model=HeroOnDB)
async def get_hero_by_id(id: ObjectId = Depends(ValidateObjectId)):
    hero = await dbase.find_one({"_id": id})
    if hero:
        return hero
    else:
        raise HTTPException(status_code=404, detail="hero not found")


@router_hero.delete("/hero/{id}", dependencies=[Depends(GetHeroOr404)], response_model=dict)
async def delete_hero_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    hero_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if hero_op.deleted_count:
        return {"status": f"deleted count: {hero_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_hero.put("/hero/{id_}", response_model=HeroOnDB)
async def update_hero(id_: str, data_in: HeroBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    hero = IsiDefault(data_in, current_user, True)
    hero.updateTime = dateTimeNow()
    if hero.tags : hero.tags = ListToUp(hero.tags)
    hero_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": hero.dict(skip_defaults=True)}
    )
    if hero_op.modified_count:
        return await GetHeroOr404(id_)
    else:
        raise HTTPException(status_code=304)
