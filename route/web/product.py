# backend/tancho/products/routes.py
import shutil
import os
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from datetime import datetime
import math
from fastapi.responses import FileResponse
from pprint import pprint

from model.web.product import ProductBase, ProductMedia, ProductOnDB, ProductPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_product = APIRouter()
dbase = MGDB.website_product

async def GetProductOr404(id: str):
    _id = ValidateObjectId(id)
    product = await dbase.find_one({"_id": _id})
    if product:
        return product
    else:
        raise HTTPException(status_code=404, detail="Product not found")

# =================================================================================
#UPLOAD UPDATE SHOW IMAGE

#UPLOAD IMAGE
@router_product.post("/product/upload_gambar")
async def upload_gambar(
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "product"
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)

#EDIT IMAGE
@router_product.post("/product/upload_gambar/edit/{id}")
async def edit_gambar(
    id: str,
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "product"

    pipeline = [
        {"$match": {"_id": ObjectId(id)}},
        {"$unwind": {"path": "$image"}},
        {"$project": {
            "image": 1
        }}
    ]
    file = await dbase.aggregate(pipeline).to_list(1000)
    filename = file[0]['image']['filename']
    pprint(filename)
    img = os.path.join(path, path2, filename)
    if os.path.exists(img):
        os.remove(img)

    data_in = ProductMedia(
        filename=gambar.filename,
        contentType=gambar.content_type
    )
    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$pull": {"image": {"filename": filename}}},
    )

    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$addToSet": {"image": data_in.dict()}},
    )
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)


#GET IMAGE
@router_product.get("/product/get_gambar/{filename}")
async def get_upload_gambar(
    filename: str
):
    path = "/app/upload-image"
    path2 = "product"
    file_path = os.path.join(path, path2, filename)
    return FileResponse(file_path)

# =================================================================================

@router_product.post("/product", response_model=ProductOnDB)
async def add_product(data_in: ProductBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    product = IsiDefault(data_in, current_user)
    product.tags = ListToUp(product.tags)
    product.submitdate = dateTimeNow()
    product_op = await dbase.insert_one(product.dict())
    if product_op.inserted_id:
        product = await GetProductOr404(product_op.inserted_id)
        return product


@router_product.post("/get_product/{companyId}", response_model=dict)
async def get_all_products(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = ProductPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_product.get("/product/{id}", response_model=ProductOnDB)
async def get_product_by_id(id: ObjectId = Depends(ValidateObjectId)):
    product = await dbase.find_one({"_id": id})
    if product:
        return product
    else:
        raise HTTPException(status_code=404, detail="Product not found")


@router_product.delete("/product/{id}", dependencies=[Depends(GetProductOr404)], response_model=dict)
async def delete_product_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    product_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if product_op.deleted_count:
        return {"status": f"deleted count: {product_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_product.put("/product/{id_}", response_model=ProductOnDB)
async def update_product(id_: str, data_in: ProductBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    product = IsiDefault(data_in, current_user, True)
    product.updateTime = dateTimeNow()
    if product.tags : product.tags = ListToUp(product.tags)
    product_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": product.dict(skip_defaults=True)}
    )
    if product_op.modified_count:
        return await GetProductOr404(id_)
    else:
        raise HTTPException(status_code=304)