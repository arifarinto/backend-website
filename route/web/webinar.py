import os
import shutil
from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, UploadFile, File, Form
from typing import List, Optional
from fastapi.responses import FileResponse
from datetime import datetime
import math

from model.web.webinar import WebinarBase, WebinarOnDB, WebinarPage, PesertaWebinar, WebinarMedia, WebinarPageOut, pageReportPeserta, pageReportPesertaOut, pageWebinarPesertaList
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp, RandomNumber
from util.util_waktu import convertDateToStrDate, dateTimeNow
from model.default import JwtToken
from util.util_waktu import convertStrDateTimeToDateTime

from pprint import pprint

router_webinar = APIRouter()
dbase = MGDB.website_webinar


async def GetWebinarOr404(id: str):
    _id = ValidateObjectId(id)
    webinar = await dbase.find_one({"_id": _id})
    if webinar:
        return webinar
    else:
        raise HTTPException(status_code=404, detail="Webinar not found")

# =================================================================================


@router_webinar.post("/webinar/upload_gambar")
async def upload_gambar(
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "webinar"
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)


@router_webinar.post("/webinar/upload_gambar/edit/{id}")
async def edit_gambar(
    id: str,
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "webinar"

    pipeline = [
        {"$match": {"_id": ObjectId(id)}},
        {"$unwind": {"path": "$image"}},
        {"$project": {
            "image": 1
        }}
    ]
    file = await dbase.aggregate(pipeline).to_list(1000)
    filename = file[0]['image']['filename']
    pprint(filename)
    img = os.path.join(path, path2, filename)
    if os.path.exists(img):
        os.remove(img)

    data_in = WebinarMedia(
        filename=gambar.filename,
        contentType=gambar.content_type
    )
    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$pull": {"image": {"filename": filename}}},
    )

    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$addToSet": {"image": data_in.dict()}},
    )
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)


@router_webinar.get("/webinar/get_gambar/{filename}")
async def get_upload_gambar(
    filename: str
):
    path = "/app/upload-image"
    path2 = "webinar"
    file_path = os.path.join(path, path2, filename)
    return FileResponse(file_path)


@router_webinar.post("/webinar", response_model=WebinarOnDB)
async def add_webinar(
    data_in: WebinarBase,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    webinar = IsiDefault(data_in, current_user)
    webinar.tanggal = convertDateToStrDate(webinar.tanggal)
    webinar.jam = str(webinar.jam)
    webinar.waktu = convertStrDateTimeToDateTime(f"{webinar.tanggal} {webinar.jam}")
    webinar.tags = ListToUp(webinar.tags)
    # tanggal = datetime.strftime(webinar.waktu, "%H:%M:%S")
    # print(tanggal)
    # print(data_in)

    webinar_op = await dbase.insert_one(webinar.dict())
    if webinar_op.inserted_id:
        webinar = await GetWebinarOr404(webinar_op.inserted_id)
        return webinar


# @router_webinar.post("/get_webinar", response_model=dict)
# async def get_all_webinar(size: int = 10, page: int = 0, sort: str = "updateTime", dir: int = -1):
#     skip = page * size
#     # criteria = CreateCriteria(search)

#     pipeline = [
#     {
#         '$project': {
#             'companyId': 1,
#             'creatorUserId': 1, 
#             'judul': 1, 
#             'description': 1, 
#             'urlWebinar': 1, 
#             'urlYoutube': 1, 
#             'urlGroup': 1, 
#             'jam': 1, 
#             'waktu': {'$dateToString': {'format': '%d-%m-%Y', 'date': '$waktu'}},
#             'image': 1
#         }
#     }
# ]

#     webinar = await dbase.aggregate(pipeline).to_list(10000)

#     datas_cursor = dbase.find().skip(skip).limit(size).sort(sort, dir)
#     datas = await datas_cursor.to_list(length=size)
#     print(datas)
#     # totalElements = await dbase.count_documents(criteria)
#     # totalPages = math.ceil(totalElements / size)
#     reply = WebinarPageOut(
#         content=webinar, 
#         size=size, 
#         page=page, 
#         sortDirection=dir)
#     return reply

@router_webinar.post("/get_webinar", response_model=dict)
async def get_all_webinar(size: int = 10, page: int = 0, sort: str = "updateTime", dir: int = -1):
    skip = page * size
    # criteria = CreateCriteria(search)
    datas_cursor = dbase.find().skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    print(datas)
    # totalElements = await dbase.count_documents(criteria)
    # totalPages = math.ceil(totalElements / size)
    reply = WebinarPage(content=datas, size=size, page=page, sortDirection=dir)
    return reply


@router_webinar.get("/webinar/{id}", response_model=WebinarOnDB)
async def get_webinar_by_id(id: ObjectId = Depends(ValidateObjectId)):
    webinar = await dbase.find_one({"_id": id})
    if webinar:
        return webinar
    else:
        raise HTTPException(status_code=404, detail="Webinar not found")


@router_webinar.delete("/webinar/{id}", dependencies=[Depends(GetWebinarOr404)], response_model=dict)
async def delete_webinar_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    webinar = await dbase.find_one({"_id": ObjectId(id)})
    folder = "/app/upload-image/webinar"
    print(webinar["image"][0]["filename"])
    img = os.path.join(folder, webinar["image"][0]["filename"])
    os.remove(img)
    webinar_op = await dbase.delete_one({"_id": ObjectId(id), "companyId": ObjectId(current_user.companyId)})
    if webinar_op.deleted_count:
        return {"status": f"deleted count: {webinar_op.deleted_count}"}
    else:
        raise HTTPException(
            status_code=404, detail="Delete failed, ID Not Found")


@router_webinar.put("/webinar/{id_}", response_model=WebinarOnDB)
async def update_webinar(id_: str, data_in: WebinarBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    webinar = IsiDefault(data_in, current_user, True)
    webinar.updateTime = dateTimeNow()
    webinar.tanggal = convertDateToStrDate(webinar.tanggal)
    webinar.jam = str(webinar.jam)
    if webinar.tags:
        webinar.tags = ListToUp(webinar.tags)
    webinar_op = await dbase.update_one(
        {"_id": ObjectId(id_), "companyId": ObjectId(current_user.companyId)},
        {"$set": webinar.dict(skip_defaults=True)}
    )
    if webinar_op.modified_count:
        return await GetWebinarOr404(id_)
    else:
        raise HTTPException(status_code=304)

# =================================================================================
# create and get data peserta webinar

# post peserta webinar


@router_webinar.post("/webinar/daftar/{idWebinar}", response_model=PesertaWebinar)
async def add_pesertawebinar(idWebinar: str, data_in: PesertaWebinar):

    data_in.provinsi = data_in.provinsi.upper()
    data_in.kota = data_in.kota.upper()
    data_in.nama = data_in.nama.upper()
    data_in.idPeserta = RandomNumber(6)
    data_in.tglsubmit = dateTimeNow()
    pipeline = [
        {"$match": {"_id": ObjectId(idWebinar)}},
        {"$unwind": {"path": "$peserta"}},
        {"$replaceRoot": {"newRoot": "$peserta"}},
        {"$match": {"idPeserta": data_in.idPeserta}},
    ]
    peserta = await dbase.aggregate(pipeline).to_list(1000)
    if peserta:
        raise HTTPException(
            status_code=400, detail=f"id untuk peserta dengan id {data_in.idPeserta} sudah ada")

    peserta_op = await dbase.update_one(
        {"_id": ObjectId(idWebinar)},
        {"$addToSet": {"peserta": data_in.dict()}}
    )
    if peserta_op.modified_count:
        return data_in
    else:
        raise HTTPException(status_code=304)


# get all peserta webinar
@router_webinar.get("/webinar/daftar/get_peserta/{idWebinar}", response_model=dict)
async def get_all_peserta(
    idWebinar: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    pipeline = [
        {"$unwind": {"path": "$peserta"}},
        {"$match": {"_id": ObjectId(idWebinar)}},
        {"$replaceRoot": {"newRoot": "$peserta"}},
    ]
    peserta = await dbase.aggregate(pipeline).to_list(10000)
    if not peserta:
        raise HTTPException(status_code=404, detail="peserta tidak ditemukan")
    data_peserta = {}
    data_peserta["_id"] = idWebinar
    data_peserta['peserta'] = peserta
    return data_peserta


@router_webinar.post("/get_peserta_webinar", response_model=dict)
async def get_all_webinar(size: int = 10, page: int = 0, sort: str = "updateTime", dir: int = -1):
    skip = page * size
    # criteria = CreateCriteria(search)
    pipeline = [
        {"$project": {
            "judul": 1,
            "tanggal": 1,
            "peserta": 1,
            "total": {"$cond": {"if": {"$isArray": "$peserta"}, "then": {"$size": "$peserta"}, "else": "NA"}}
        }}
    ]

    datas = await dbase.aggregate(pipeline).to_list(10000)
    # pprint(data)
    # totalElements = await dbase.count_documents(criteria)
    # totalPages = math.ceil(totalElements / size)
    reply = pageWebinarPesertaList(
        content=datas, size=size, page=page, sortDirection=dir)
    return reply

# =================================================================================
# DOWNLOAD EXCEL DATA PESERTA

# REPORT PER-WEBINAR


@router_webinar.post("/peserta_webinar/report/{idWebinar}")
async def get_report_by_idWebinar(
    idWebinar: str,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    pipeline = [
        {"$match": {"_id": ObjectId(idWebinar)}},
        {"$unwind": {"path": "$peserta"}},
        {'$set': {
            'peserta.tglsubmit': {'$dateToString': {'format': '%Y-%m-%d', 'date': '$peserta.tglsubmit'}}
        }},
        {"$project": {
            "judul": 1,
            "description": 1,
            "tanggal": 1,
            "peserta": 1,
        }
        }
    ]

    peserta_webinar = await dbase.aggregate(pipeline).to_list(10000)
    file = peserta_webinar

    pprint(file)

    reply = pageReportPesertaOut(
        content=file
    )

    pprint(reply)
    return reply

# REPORT PER-BULAN

@router_webinar.post("/peserta_webinar/report/bulan/{month}")
async def get_report_by_bulan (
    month : int,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    pipeline = [
            {"$unwind": {"path": "$peserta"}},
            {'$set': {
            'peserta.tglsubmit': {'$dateToString': {'format': '%Y-%m-%d', 'date': '$peserta.tglsubmit'}}
            }},
            {"$project": {
                "judul": 1,
                "description": 1,
                "peserta": 1,
                "bulan": {"$month": {"date": "$waktu"}},
                }
            },
            {"$match": {"bulan": month}}
        ]
    PesertaWebinar = await dbase.aggregate(pipeline).to_list(10000)
    file = PesertaWebinar

    pprint(file)
    reply = pageReportPesertaOut(
        content=file
    )

    return reply

# REPORT PER-TAHUN
# @router_webinar.post("/peserta_webinar/report/tahun/{year}")
# async def get_report_by_tahun (
#     year : int,
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
# ):
#     pipeline = [
#             {"$unwind": {"path": "$peserta"}},
#             {'$set': {
#             'peserta.tglsubmit': {'$dateToString': {'format': '%Y-%m-%d', 'date': '$peserta.tglsubmit'}}
#             }},
#             {"$project": {
#                 "judul": 1,
#                 "description": 1,
#                 "peserta": 1,
#                 "waktu":1,
#                 "tahun": {"$year": {"date": "$waktu"}},
#                 }
#             },
#             {"$match": {"tahun": year}}
#         ]
#     PesertaWebinar = await dbase.aggregate(pipeline).to_list(10000)
#     file = PesertaWebinar

#     reply = pageReportPesertaOut(
#         content=PesertaWebinar
#     )

#     # pprint(reply)
#     return reply
