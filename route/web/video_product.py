from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, File, UploadFile
from typing import List
from datetime import datetime
import math
import os
import shutil
from fastapi.responses import FileResponse
from pprint import pprint

from model.web.video_product import VideoProductBase, VideoProductMedia, VideoProductOnDB, VideoProductPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_video_product = APIRouter()
dbase = MGDB.website_video_product

async def GetVideoProductOr404(id: str):
    _id = ValidateObjectId(id)
    video_product = await dbase.find_one({"_id": _id})
    if video_product:
        return video_product
    else:
        raise HTTPException(status_code=404, detail="VideoProduct not found")

# =================================================================================
#UPLOAD UPDATE SHOW IMAGE

#UPLOAD IMAGE
@router_video_product.post("/video_product/upload_gambar")
async def upload_gambar(
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "product"
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)

#EDIT IMAGE
@router_video_product.post("/video_product/upload_gambar/edit/{id}")
async def edit_gambar(
    id: str,
    gambar: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
):
    path = "/app/upload-image"
    path2 = "product"

    pipeline = [
        {"$match": {"_id": ObjectId(id)}},
        {"$unwind": {"path": "$image"}},
        {"$project": {
            "image": 1
        }}
    ]
    file = await dbase.aggregate(pipeline).to_list(1000)
    filename = file[0]['image']['filename']
    pprint(filename)
    img = os.path.join(path, path2, filename)
    if os.path.exists(img):
        os.remove(img)

    data_in = VideoProductMedia(
        filename=gambar.filename,
        contentType=gambar.content_type
    )
    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$pull": {"image": {"filename": filename}}},
    )

    peminjaman_op = await dbase.update_one(
        {"_id": ObjectId(id)},
        {"$addToSet": {"image": data_in.dict()}},
    )
    with open(os.path.join(path, path2, gambar.filename), "wb") as imgBuffer:
        shutil.copyfileobj(gambar.file, imgBuffer)


#GET IMAGE
@router_video_product.get("/video_product/get_gambar/{filename}")
async def get_upload_gambar(
    filename: str
):
    path = "/app/upload-image"
    path2 = "product"
    file_path = os.path.join(path, path2, filename)
    return FileResponse(file_path)

# =================================================================================

@router_video_product.post("/video_product", response_model=VideoProductOnDB)
async def add_video_product(data_in: VideoProductBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    video_product = IsiDefault(data_in, current_user)
    video_product.tags = ListToUp(video_product.tags)
    video_product_op = await dbase.insert_one(video_product.dict())
    if video_product_op.inserted_id:
        video_product = await GetVideoProductOr404(video_product_op.inserted_id)
        return video_product


@router_video_product.post("/get_video_product/{companyId}", response_model=dict)
async def get_all_video_products(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = VideoProductPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_video_product.get("/video_product/{id}", response_model=VideoProductOnDB)
async def get_video_product_by_id(id: ObjectId = Depends(ValidateObjectId)):
    video_product = await dbase.find_one({"_id": id})
    if video_product:
        return video_product
    else:
        raise HTTPException(status_code=404, detail="VideoProduct not found")


@router_video_product.delete("/video_product/{id}", dependencies=[Depends(GetVideoProductOr404)], response_model=dict)
async def delete_video_product_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    video_product_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if video_product_op.deleted_count:
        return {"status": f"deleted count: {video_product_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_video_product.put("/video_product/{id_}", response_model=VideoProductOnDB)
async def update_video_product(id_: str, data_in: VideoProductBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    video_product = IsiDefault(data_in, current_user, True)
    video_product.updateTime = dateTimeNow()
    if video_product.tags : video_product.tags = ListToUp(video_product.tags)
    video_product_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": video_product.dict(skip_defaults=True)}
    )
    if video_product_op.modified_count:
        return await GetVideoProductOr404(id_)
    else:
        raise HTTPException(status_code=304)
