from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.achievement import AchievementBase, AchievementOnDB, AchievementPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_achievement = APIRouter()
dbase = MGDB.website_achievement

async def GetAchievementOr404(id: str):
    _id = ValidateObjectId(id)
    achievement = await dbase.find_one({"_id": _id})
    if achievement:
        return achievement
    else:
        raise HTTPException(status_code=404, detail="Achievement not found")

# =================================================================================

@router_achievement.post("/achievement", response_model=AchievementOnDB)
async def add_achievement(data_in: AchievementBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    achievement = IsiDefault(data_in, current_user)
    achievement.tags = ListToUp(achievement.tags)
    achievement_op = await dbase.insert_one(achievement.dict())
    if achievement_op.inserted_id:
        achievement = await GetAchievementOr404(achievement_op.inserted_id)
        return achievement


@router_achievement.post("/get_achievement/{companyId}", response_model=dict)
async def get_all_achievements(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = AchievementPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_achievement.get("/achievement/{id}", response_model=AchievementOnDB)
async def get_achievement_by_id(id: ObjectId = Depends(ValidateObjectId)):
    achievement = await dbase.find_one({"_id": id})
    if achievement:
        return achievement
    else:
        raise HTTPException(status_code=404, detail="Achievement not found")


@router_achievement.delete("/achievement/{id}", dependencies=[Depends(GetAchievementOr404)], response_model=dict)
async def delete_achievement_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    achievement_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if achievement_op.deleted_count:
        return {"status": f"deleted count: {achievement_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_achievement.put("/achievement/{id_}", response_model=AchievementOnDB)
async def update_achievement(id_: str, data_in: AchievementBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    achievement = IsiDefault(data_in, current_user, True)
    achievement.updateTime = dateTimeNow()
    if achievement.tags : achievement.tags = ListToUp(achievement.tags)
    achievement_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": achievement.dict(skip_defaults=True)}
    )
    if achievement_op.modified_count:
        return await GetAchievementOr404(id_)
    else:
        raise HTTPException(status_code=304)
