from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.partner import PartnerBase, PartnerOnDB, PartnerPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_partner = APIRouter()
dbase = MGDB.website_partner

async def GetPartnerOr404(id: str):
    _id = ValidateObjectId(id)
    partner = await dbase.find_one({"_id": _id})
    if partner:
        return partner
    else:
        raise HTTPException(status_code=404, detail="Partner not found")

# =================================================================================

@router_partner.post("/partner", response_model=PartnerOnDB)
async def add_partner(data_in: PartnerBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    partner = IsiDefault(data_in, current_user)
    partner.tags = ListToUp(partner.tags)
    partner_op = await dbase.insert_one(partner.dict())
    if partner_op.inserted_id:
        partner = await GetPartnerOr404(partner_op.inserted_id)
        return partner


@router_partner.post("/get_partner/{companyId}", response_model=dict)
async def get_all_partners(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = PartnerPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_partner.get("/partner/{id}", response_model=PartnerOnDB)
async def get_partner_by_id(id: ObjectId = Depends(ValidateObjectId)):
    partner = await dbase.find_one({"_id": id})
    if partner:
        return partner
    else:
        raise HTTPException(status_code=404, detail="Partner not found")


@router_partner.delete("/partner/{id}", dependencies=[Depends(GetPartnerOr404)], response_model=dict)
async def delete_partner_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    partner_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if partner_op.deleted_count:
        return {"status": f"deleted count: {partner_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_partner.put("/partner/{id_}", response_model=PartnerOnDB)
async def update_partner(id_: str, data_in: PartnerBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    partner = IsiDefault(data_in, current_user, True)
    partner.updateTime = dateTimeNow()
    if partner.tags : partner.tags = ListToUp(partner.tags)
    partner_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": partner.dict(skip_defaults=True)}
    )
    if partner_op.modified_count:
        return await GetPartnerOr404(id_)
    else:
        raise HTTPException(status_code=304)
