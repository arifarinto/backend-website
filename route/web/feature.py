from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.feature import FeatureBase, FeatureOnDB, FeaturePage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_feature = APIRouter()
dbase = MGDB.website_feature

async def GetFeatureOr404(id: str):
    _id = ValidateObjectId(id)
    feature = await dbase.find_one({"_id": _id})
    if feature:
        return feature
    else:
        raise HTTPException(status_code=404, detail="Feature not found")

# =================================================================================

@router_feature.post("/feature", response_model=FeatureOnDB)
async def add_feature(data_in: FeatureBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    feature = IsiDefault(data_in, current_user)
    feature.tags = ListToUp(feature.tags)
    feature_op = await dbase.insert_one(feature.dict())
    if feature_op.inserted_id:
        feature = await GetFeatureOr404(feature_op.inserted_id)
        return feature


@router_feature.post("/get_feature/{companyId}", response_model=dict)
async def get_all_features(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = FeaturePage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_feature.get("/feature/{id}", response_model=FeatureOnDB)
async def get_feature_by_id(id: ObjectId = Depends(ValidateObjectId)):
    feature = await dbase.find_one({"_id": id})
    if feature:
        return feature
    else:
        raise HTTPException(status_code=404, detail="Feature not found")


@router_feature.delete("/feature/{id}", dependencies=[Depends(GetFeatureOr404)], response_model=dict)
async def delete_feature_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    feature_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if feature_op.deleted_count:
        return {"status": f"deleted count: {feature_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_feature.put("/feature/{id_}", response_model=FeatureOnDB)
async def update_feature(id_: str, data_in: FeatureBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    feature = IsiDefault(data_in, current_user, True)
    feature.updateTime = dateTimeNow()
    if feature.tags : feature.tags = ListToUp(feature.tags)
    feature_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": feature.dict(skip_defaults=True)}
    )
    if feature_op.modified_count:
        return await GetFeatureOr404(id_)
    else:
        raise HTTPException(status_code=304)
