from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from typing import List
from datetime import datetime
import math

from model.web.pricing import PricingBase, PricingOnDB, PricingPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import dateTimeNow

router_pricing = APIRouter()
dbase = MGDB.website_pricing

async def GetPricingOr404(id: str):
    _id = ValidateObjectId(id)
    pricing = await dbase.find_one({"_id": _id})
    if pricing:
        return pricing
    else:
        raise HTTPException(status_code=404, detail="Pricing not found")

# =================================================================================

@router_pricing.post("/pricing", response_model=PricingOnDB)
async def add_pricing(data_in: PricingBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pricing = IsiDefault(data_in, current_user)
    pricing.tags = ListToUp(pricing.tags)
    pricing_op = await dbase.insert_one(pricing.dict())
    if pricing_op.inserted_id:
        pricing = await GetPricingOr404(pricing_op.inserted_id)
        return pricing


@router_pricing.post("/get_pricing/{companyId}", response_model=dict)
async def get_all_pricings(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = PricingPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_pricing.get("/pricing/{id}", response_model=PricingOnDB)
async def get_pricing_by_id(id: ObjectId = Depends(ValidateObjectId)):
    pricing = await dbase.find_one({"_id": id})
    if pricing:
        return pricing
    else:
        raise HTTPException(status_code=404, detail="Pricing not found")


@router_pricing.delete("/pricing/{id}", dependencies=[Depends(GetPricingOr404)], response_model=dict)
async def delete_pricing_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pricing_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if pricing_op.deleted_count:
        return {"status": f"deleted count: {pricing_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_pricing.put("/pricing/{id_}", response_model=PricingOnDB)
async def update_pricing(id_: str, data_in: PricingBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    pricing = IsiDefault(data_in, current_user, True)
    pricing.updateTime = dateTimeNow()
    if pricing.tags : pricing.tags = ListToUp(pricing.tags)
    pricing_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": pricing.dict(skip_defaults=True)}
    )
    if pricing_op.modified_count:
        return await GetPricingOr404(id_)
    else:
        raise HTTPException(status_code=304)
