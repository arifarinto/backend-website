# backend/tancho/testimonys/routes.py

from bson.objectid import ObjectId
from starlette.requests import Request
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from datetime import datetime
import math

from model.web.testimony import TestimonyBase, TestimonyOnDB, TestimonyPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_testimony = APIRouter()
dbase = MGDB.website_testimony

async def GetTestimonyOr404(id: str):
    _id = ValidateObjectId(id)
    testimony = await dbase.find_one({"_id": _id})
    if testimony:
        return testimony
    else:
        raise HTTPException(status_code=404, detail="Testimony not found")

# =================================================================================

@router_testimony.post("/testimony", response_model=TestimonyOnDB)
async def add_testimony(data_in: TestimonyBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    testimony = IsiDefault(data_in, current_user)
    testimony.nama = testimony.nama.upper()
    testimony.tags = ListToUp(testimony.tags)
    testimony_op = await dbase.insert_one(testimony.dict())
    if testimony_op.inserted_id:
        testimony = await GetTestimonyOr404(testimony_op.inserted_id)
        return testimony


# @router_testimony.post("/testimony", response_model=TestimonyOnDB)
# async def add_testimony(
#     data_in: TestimonyBase,
#     request: Request =None 
# # current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]
# ):
#     token = await request.json()
#     current_user = await get_current_user(["*", "*"], token=token["token"])
#     testimony = IsiDefault(data_in, current_user)
#     testimony.nama = testimony.nama.upper()
#     testimony.tags = ListToUp(testimony.tags)
#     testimony_op = await dbase.insert_one(testimony.dict())
#     if testimony_op.inserted_id:
#         testimony = await GetTestimonyOr404(testimony_op.inserted_id)
#         return testimony


@router_testimony.post("/get_testimony/{companyId}", response_model=dict)
async def get_all_testimony(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = TestimonyPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_testimony.get("/testimony/{id}", response_model=TestimonyOnDB)
async def get_testimony_by_id(id: ObjectId = Depends(ValidateObjectId)):
    testimony = await dbase.find_one({"_id": id})
    if testimony:
        return testimony
    else:
        raise HTTPException(status_code=404, detail="Testimony not found")


@router_testimony.delete("/testimony/{id}", dependencies=[Depends(GetTestimonyOr404)], response_model=dict)
async def delete_testimony_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    testimony_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if testimony_op.deleted_count:
        return {"status": f"deleted count: {testimony_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_testimony.put("/testimony/{id_}", response_model=TestimonyOnDB)
async def update_testimony(id_: str, data_in: TestimonyBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    testimony = IsiDefault(data_in, current_user, True)
    testimony.updateTime = dateTimeNow()
    testimony.nama = testimony.nama.upper()
    if testimony.tags : testimony.tags = ListToUp(testimony.tags)
    testimony_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": testimony.dict(skip_defaults=True)}
    )
    if testimony_op.modified_count:
        return await GetTestimonyOr404(id_)
    else:
        raise HTTPException(status_code=304)