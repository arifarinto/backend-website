from bson.objectid import ObjectId
from config.config import CONF, MGDB
from fastapi import APIRouter, Depends, HTTPException, Security, Request
from typing import List
from datetime import datetime
import math
import shutil
from fastapi.datastructures import UploadFile
from fastapi.param_functions import File

from model.web.karir import KarirBase, KarirOnDB, KarirPage, StatusEnum
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, RandomNumber, ValidateObjectId, CreateCriteria, FieldObjectIdRequest, ListToUp
from util.util_waktu import convertDateToStrDate, dateTimeNow

router_karir = APIRouter()
dbase = MGDB.website_karir

async def GetKarirOr404(id: str):
    _id = ValidateObjectId(id)
    karir = await dbase.find_one({"_id": _id})
    if karir:
        return karir
    else:
        raise HTTPException(status_code=404, detail="Karir not found")

# =================================================================================
# CRUD KARIR

@router_karir.get("/combo_status",response_model=list)
async def get_combo_status():
    return list(StatusEnum)


@router_karir.post("/karir", response_model=KarirOnDB)
async def add_karir(data_in: KarirBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    karir = IsiDefault(data_in, current_user)
    karir.position = karir.position.upper()
    karir.deadline = convertDateToStrDate(karir.deadline)
    karir.tags = ListToUp(karir.tags)
    karir.requirement = ListToUp(karir.requirement)
    karir.jobdesk = ListToUp(karir.jobdesk)
    karir.benefit = ListToUp(karir.benefit)
    karir.submitdate = dateTimeNow()
    karir_op = await dbase.insert_one(karir.dict())
    if karir_op.inserted_id:
        karir = await GetKarirOr404(karir_op.inserted_id)
        return karir


@router_karir.post("/get_karir/{companyId}", response_model=dict)
async def get_all_karirs(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None, request: Request = None):
    skip = page * size
    print(search)
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = KarirPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_karir.get("/karir/{id}", response_model=KarirOnDB)
async def get_karir_by_id(id: ObjectId = Depends(ValidateObjectId)):
    karir = await dbase.find_one({"_id": id})
    if karir:
        return karir
    else:
        raise HTTPException(status_code=404, detail="Karir not found")


@router_karir.delete("/karir/{id}", dependencies=[Depends(GetKarirOr404)], response_model=dict)
async def delete_karir_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    karir_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if karir_op.deleted_count:
        return {"status": f"deleted count: {karir_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_karir.put("/karir/{id_}", response_model=KarirOnDB)
async def update_karir(id_: str, data_in: KarirBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    karir = IsiDefault(data_in, current_user, True)
    karir.updateTime = dateTimeNow()
    karir.position = karir.position.upper()
    karir.deadline = convertDateToStrDate(karir.deadline)
    if karir.tags : karir.tags = ListToUp(karir.tags)
    if karir.requirement : karir.requirement = ListToUp(karir.requirement)
    if karir.jobdesk : karir.jobdesk = ListToUp(karir.jobdesk)
    if karir.benefit : karir.benefit = ListToUp(karir.benefit)
    karir_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": karir.dict(skip_defaults=True)}
    )
    if karir_op.modified_count:
        return await GetKarirOr404(id_)
    else:
        raise HTTPException(status_code=304)


# =================================================================================
# CRUD PELAMAR

# @router_karir.get("/combo_gender",response_model=list)
# async def get_combo_gender():
#     return list(GenEnum)


# @router_karir.get("/combo_education",response_model=list)
# async def get_combo_last_education():
#     return list(LastEducateEnum)


# @router_karir.post("/karir/pelamar/{idKarir}", response_model=PelamarData)
# async def add_pelamar(
#     idKarir: str,
#     dataIn: PelamarData, 
    # current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
# ):
    # proteksi tgl apply dengan deadline loker
    # karir = await dbase.find_one({"_id": ObjectId(idKarir),"companyId": ObjectId(current_user.companyId)})
    # if karir:
    #     loker = karir
    # else:
    #     raise HTTPException(status_code=404, detail="loker tidak ditemukan")
    # deadline = loker["deadline"]
    # dataIn.tglApply = dateTimeNow()
    # apply = datetime.strptime(dataIn.tglApply, '%Y-%m-%d %H:%M:%D')
    # if apply > deadline:
    #     raise HTTPException (status_code=400, detail="deadline loker ini telah habis")

#     dataIn.firstName = dataIn.firstName.upper()
#     dataIn.lastName = dataIn.lastName.upper()
#     dataIn.idPelamar = RandomNumber(6)
#     dataIn.tglApply = dateTimeNow()
#     dataIn.tglLahir = convertDateToStrDate (dataIn.tglLahir)
#     dataIn.startDate = convertDateToStrDate (dataIn.startDate)
#     pipeline = [
#             {"$match": {"_id": ObjectId(idKarir)}},
#             {"$unwind": {"path": "$pelamar"}},
#             {"$replaceRoot": {"newRoot": "$pelamar"}},
#             {"$match": {"idPelamar": dataIn.idPelamar}},
#     ]
#     pelamar = await dbase.aggregate(pipeline).to_list(1000)
#     if pelamar:
#         raise HTTPException(status_code=400, detail=f"id untuk pelamar dengan id {dataIn.idPelamar} sudah ada")

#     pelamar_op = await dbase.update_one(
#                 { "_id": ObjectId(idKarir)},
#                 { "$addToSet": { "pelamar": dataIn.dict()}}
#             )
#     if pelamar_op.modified_count:
#         return dataIn
#     else:
#         raise HTTPException(status_code=304)


# @router_karir.get("/karir/pelamar/get_pelamar/{idKarir}", response_model=dict)
# async def get_all_pelamar(
#     idKarir: str,
    # current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
# ):
    # pipeline = [
    #         {"$unwind": {"path": "$pelamar"}},
    #         {"$match": {"_id": ObjectId(idKarir)}},
    #         {"$replaceRoot": {"newRoot": "$pelamar"}},
    #     ]
    # pelamar = await dbase.aggregate(pipeline).to_list(10000)
    # if not pelamar:
    #     raise HTTPException (status_code=404, detail= "pelamar tidak ditemukan")
    # data_pelamar = {}
    # data_pelamar["karir"] = idKarir
    # data_pelamar['pelamar'] = pelamar
    # return data_pelamar


# @router_karir.get("/karir/pelamar/{idKarir}/{idPelamar}", response_model=PelamarData)
# async def get_pelamar_by_id(
#     idKarir: str, 
#     idPelamar: str, 
    # current_user: JwtToken = Security(get_current_user, scopes=["*","*"])
# ):
    # pipeline = [
    #     {"$match": {"_id": ObjectId(idKarir)}},
    #     {"$unwind": {"path": "$pelamar"}},
    #     {"$replaceRoot": {"newRoot": "$pelamar"}},
    #     {"$match": {"idPelamar": idPelamar}},
    # ]
    # result = await dbase.aggregate(pipeline).to_list(1000)
    # if result:
    #     pelamar = result[0]
    #     return pelamar
    # else:
    #     raise HTTPException (status_code=404, detail= "pelamar tidak ditemukan")


# @router_karir.delete("/karir/pelamar/{idKarir}/{idPelamar}", response_model=dict)
# async def delete_pelamar(
#     idKarir: str, 
#     idPelamar: str, 
    # current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
# ):
    # pipeline = [
    #     {"$match": {"_id": ObjectId(idKarir)}},
    #     {"$unwind": {"path": "$pelamar"}},
    #     {"$replaceRoot": {"newRoot": "$pelamar"}},
    #     {"$match": {"idPelamar": idPelamar}}
    # ]
    # result = await dbase.aggregate(pipeline).to_list(1000)
    # if not result:
    #     raise HTTPException (status_code=404, detail= "pelamar tidak ditemukan")

    # pelamar_op = await dbase.update_one(
    #     {"_id": ObjectId(idKarir),"pelamar.idPelamar": idPelamar},
    #     {"$pull":{"pelamar": {"idPelamar": idPelamar}}}
    # )
    # if pelamar_op.modified_count:
    #     return {"status": f"deleted count: {pelamar_op.modified_count}"}
    # else:
    #     raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


# @router_karir.put("/karir/pelamar/{idKarir}/{idPelamar}", response_model=dict)
# async def update_pelamar(
#     idKarir: str, 
#     idPelamar: str, 
#     dataIn: PelamarData, 
    # current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])
# ):
#     dataIn.idPelamar = idPelamar
#     dataIn.firstName = dataIn.firstName.upper()
#     dataIn.lastName = dataIn.lastName.upper()
#     dataIn.tglApply = dateTimeNow()
#     dataIn.tglLahir = convertDateToStrDate (dataIn.tglLahir)
#     dataIn.startDate = convertDateToStrDate (dataIn.startDate)
    
#     pipeline = [
#         {"$match": {"_id": ObjectId(idKarir)}},
#         {"$unwind": {"path": "$pelamar"}},
#         {"$replaceRoot": {"newRoot": "$pelamar"}},
#         {"$match": {"idPelamar": idPelamar}},
#     ]
#     result = await dbase.aggregate(pipeline).to_list(1000)
#     if not result:
#         raise HTTPException (status_code=404, detail= "pelamar tidak ditemukan")
#     pelamar = result[0]
#     dataIn = dataIn.dict(skip_defaults=True)
#     dataIn = {k: v for k, v in dataIn.items() if v is not None}
#     pelamar.update(dataIn)
#     pelamar_op = await dbase.update_one(
#         {"_id": ObjectId(idKarir), "pelamar.idPelamar": idPelamar},
#         {"$set": {"pelamar.$": pelamar}},
#     )
#     if pelamar_op.modified_count:
#         return pelamar
#     else:
#         raise HTTPException(status_code=304)

# @router_karir.post("/upload_cv")
# async def upload_cv(
#     file: UploadFile = File(...),
#     current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):

#     output = "upload-file/" +file.filename
#     with open(output, "wb+") as file_buffer:
#         shutil.copyfileobj(file.file, file_buffer)
#     return {"detail":"file berhasil diupload"}