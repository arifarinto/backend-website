# backend/tancho/gallerys/routes.py

from bson.objectid import ObjectId
from fastapi.datastructures import UploadFile
from fastapi.param_functions import File
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
from datetime import datetime
import math
import shutil

from model.web.gallery import GalleryBase, GalleryOnDB, GalleryPage
from model.default import JwtToken
from model.util import SearchRequest
from route.auth import get_current_user
from util.util import IsiDefault, ListToUp, ValidateObjectId, CreateCriteria, FieldObjectIdRequest
from util.util_waktu import dateTimeNow

router_gallery = APIRouter()
dbase = MGDB.website_gallery

async def GetGalleryOr404(id: str):
    _id = ValidateObjectId(id)
    gallery = await dbase.find_one({"_id": _id})
    if gallery:
        return gallery
    else:
        raise HTTPException(status_code=404, detail="Gallery not found")

# =================================================================================

@router_gallery.post("/gallery", response_model=GalleryOnDB)
async def add_gallery(data_in: GalleryBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    gallery = IsiDefault(data_in, current_user)
    gallery.tags = ListToUp(gallery.tags)
    gallery_op = await dbase.insert_one(gallery.dict())
    if gallery_op.inserted_id:
        gallery = await GetGalleryOr404(gallery_op.inserted_id)
        return gallery


@router_gallery.post("/get_gallery/{companyId}", response_model=dict)
async def get_all_gallerys(companyId:str, size: int = 10, page: int = 0, sort: str = "updateTime", dir : int = -1, search: SearchRequest = None):
    skip = page * size
    search.defaultObjectId.append(FieldObjectIdRequest(field='companyId',key=ObjectId(companyId)))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = GalleryPage(content=datas,size=size,page=page,totalElements=totalElements,totalPages=totalPages,sortDirection=dir)
    return reply


@router_gallery.get("/gallery/{id}", response_model=GalleryOnDB)
async def get_gallery_by_id(id: ObjectId = Depends(ValidateObjectId)):
    gallery = await dbase.find_one({"_id": id})
    if gallery:
        return gallery
    else:
        raise HTTPException(status_code=404, detail="Gallery not found")


@router_gallery.delete("/gallery/{id}", dependencies=[Depends(GetGalleryOr404)], response_model=dict)
async def delete_gallery_by_id(id: str, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    gallery_op = await dbase.delete_one({"_id": ObjectId(id),"companyId": ObjectId(current_user.companyId)})
    if gallery_op.deleted_count:
        return {"status": f"deleted count: {gallery_op.deleted_count}"}
    else:
        raise HTTPException(status_code=404, detail="Delete failed, ID Not Found")


@router_gallery.put("/gallery/{id_}", response_model=GalleryOnDB)
async def update_gallery(id_: str, data_in: GalleryBase, current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):
    gallery = IsiDefault(data_in, current_user, True)
    gallery.updateTime = dateTimeNow()
    if gallery.tags : gallery.tags = ListToUp(gallery.tags)
    gallery_op = await dbase.update_one(
        {"_id": ObjectId(id_),"companyId": ObjectId(current_user.companyId)}, 
        {"$set": gallery.dict(skip_defaults=True)}
    )
    if gallery_op.modified_count:
        return await GetGalleryOr404(id_)
    else:
        raise HTTPException(status_code=304)

@router_gallery.post("/upload_foto")
async def upload_foto(
    file: UploadFile = File(...),
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"])):

    output = "upload-image/" +file.filename
    with open(output, "wb+") as file_buffer:
        shutil.copyfileobj(file.file, file_buffer)
    return {"detail":"file berhasil diupload"}
        
    