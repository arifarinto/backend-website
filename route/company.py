from bson.objectid import ObjectId
from fastapi.responses import HTMLResponse
from config.config import MGDB, CONF
from fastapi import APIRouter, Depends, HTTPException, Security
import math
import json2table
import json

from model.default import (
    CompanyInput,
    IdentityData,
    JwtToken,
    CompanyBasic,
    CompanyOnDB,
    SolutionEnum,
    CompanyPage,
    UserCredential,
)
from model.util import FieldBoolRequest, SearchRequest, FieldObjectIdRequest
from function.user import CreateUser
from function.company import CreateCompany, CekDuplicate, GetCompanyOr404

from .auth import get_current_user
from util.util import (
    RandomNumber,
    RandomString,
    ValidateEmail,
    ListToUp,
    CreateCriteria,
    ValidateObjectId,
    ValidateSizePage,
)
from util.util_waktu import dateNowStr, dateTimeNow
import requests

router_company = APIRouter()
dbase = MGDB.tbl_company


@router_company.get("/combo_company_solution", response_model=list)
async def get_combo_company_solution():
    return list(SolutionEnum)


@router_company.get("/solusi_company", response_model=CompanyOnDB)
async def get_solusi_company(
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    )
):
    company = await dbase.find_one(
        {"companyId": ObjectId(current_user.companyId)},
        {"solution": 1, "mobileAppsFeature": 1},
    )
    if company:
        return company
    else:
        raise HTTPException(status_code=404, detail="Company not found")


@router_company.post("/company", response_model=CompanyOnDB)
async def add_company(
    comp: CompanyInput,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    company = CompanyBasic()
    company.createTime = dateTimeNow()
    company.updateTime = dateTimeNow()
    company.creatorUserId = ObjectId(current_user.userId)
    company.solution = comp.solution
    company.mobileAppsFeature = comp.mobileAppsFeature
    company.tags = ListToUp(comp.tags)
    company.name = comp.name.upper()
    company.picName = comp.picName.upper()
    company.picEmail = comp.picEmail.lower()
    company.picPhone = comp.picPhone
    company.initial = comp.initial.upper()
    company.address = comp.address
    await CekDuplicate(company.name, company.initial)
    ValidateEmail(company.picEmail)
    comp_op = await CreateCompany(company)

    # insert tbl user admin
    user = UserCredential()
    user.name = company.picName
    user.email = company.picEmail
    user.phone = comp.picPhone
    user.note = "default"
    arName = user.name.split(" ")
    user.username = arName[0] + RandomNumber(4)
    user.companyId = comp_op.companyId
    identity = IdentityData()
    identity.dateOfBirth = dateNowStr()
    user.identity = identity
    # await CreateUser("user_admin", user, RandomString(6), True, ['admin'])
    await CreateUser("user_admin", user, "pass", True, ["admin"])
    return await GetCompanyOr404(company.companyId)


@router_company.post("/get_company")
async def get_all_companys(
    size: int = Depends(ValidateSizePage),
    page: int = 0,
    sort: str = "companyName",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["*", "*"]),
):
    skip = page * size
    if "superadmin" not in current_user.roles:
        search.defaultObjectId.append(
            FieldObjectIdRequest(
                field="companyId", key=ObjectId(current_user.companyId)
            )
        )
    search.defaultBool.append(FieldBoolRequest(field="isDelete", key=False))
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = CompanyPage(
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_company.get("/company/{id}", response_model=CompanyOnDB)
async def get_company_by_id(
    id: ObjectId = Depends(ValidateObjectId),
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    """
    Id yang dimaksud di sini adalah atribut companyId
    """
    company = await dbase.find_one({"companyId": id, "isDelete": False})
    if company:
        return company
    else:
        raise HTTPException(status_code=404, detail="Company not found")


@router_company.get("/cek_id_by_initial/{initial}")
async def cek_id_by_initial(initial: str):
    company = await dbase.find_one(
        {"initial": initial.upper(), "isDelete": False}, {"companyId": 1}
    )
    if company:
        return {"companyId": str(company["companyId"])}
    else:
        raise HTTPException(status_code=404, detail="Company not found")


@router_company.delete("/company/{companyId}")
async def delete_company_by_id(
    companyId: str,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    await GetCompanyOr404(companyId)
    try:
        await dbase.update_one(
            {"companyId": ObjectId(companyId)}, {"$set": {"isDelete": True}}
        )
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")
    return "Company dengan companyId " + companyId + " telah dihapus"


@router_company.put("/company/{companyId}", response_model=CompanyOnDB)
async def update_company(
    companyId: str,
    company_data: CompanyBasic,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    if "superadmin" not in current_user.roles:
        companyId = current_user.companyId

    company = await dbase.find_one(
        {"companyId": ObjectId(companyId), "isDelete": False}
    )
    if company:
        company_data.updateTime = dateTimeNow()
        company_op = await dbase.update_one(
            {"companyId": ObjectId(companyId)},
            {"$set": company_data.dict(skip_defaults=True)},
        )
        if company_op.modified_count:
            company_update = await GetCompanyOr404(companyId)
            return company_update
        else:
            raise HTTPException(status_code=304)
    else:
        raise HTTPException(status_code=404)