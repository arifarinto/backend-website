import datetime
import math
from typing import List
from bson.objectid import ObjectId
from config.config import MGDB
from fastapi import APIRouter, Depends, HTTPException, Security
from function.company import GetCompanyOr404
from function.user import CreateUser, UpdateDateOfBirth
from model.default import (
    CompanyBasic,
    CompanyOnDB,
    IdentityData,
    JwtToken,
    UserBasic,
    UserCredential,
    UserDataOnDB,
    UserInput,
    UserPage,
)
from model.util import (
    FieldBoolRequest,
    FieldObjectIdRequest,
    ObjectIdStr,
    SearchRequest,
)
from route.auth import get_current_user
from util.util import CreateCriteria, RandomNumber, cleanNullTerms
from util.util_waktu import convertStrDateToDate, dateNowStr

router_admin = APIRouter()
dbase = MGDB.user_admin


async def GetAdminOr404(userId: str):
    dbase = MGDB.user_admin
    admin = await dbase.find_one({"userId": ObjectId(userId)})

    if "isDelete" in admin == False:
        admin["isDelete"] = False

    if admin and (admin["isDelete"] == False):
        return admin
    else:
        raise HTTPException(status_code=404, detail="Admin tidak ditemukan")


async def UpdateAdmin(userId: str, updateData: dict):
    await GetAdminOr404(userId)
    dbase = MGDB.user_admin
    try:
        updateData = cleanNullTerms(updateData)
        updateData = UpdateDateOfBirth(updateData)
        await dbase.update_one({"userId": ObjectId(userId)}, {"$set": updateData})
        return await GetAdminOr404(userId)
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")


@router_admin.post("/admin", response_model=UserBasic)
async def add_admin(
    adminData: UserInput,
    companyId: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    """
    Catatan:
    Phone number antara 8-13 karakter (minimal 8, maksimal 13).
    """
    print("admin data: " + str(adminData))
    company = await GetCompanyOr404(companyId)
    company = CompanyBasic(**company)
    user = UserCredential(**adminData.dict())
    print("user data: " + str(user))
    arName = user.name.split(" ")
    user.username = arName[0] + RandomNumber(4)
    user.companyId = ObjectId(companyId)
    identity = IdentityData()
    identity.dateOfBirth = dateNowStr()
    # user.identity = identity
    print("admin: " + str(user))
    return await CreateUser("user_admin", user, "pass", True, ["admin"])


@router_admin.post("/get_admin")
async def get_all_admins_by_company_id(
    companyId: ObjectIdStr,
    size: int = 10,
    page: int = 0,
    sort: str = "updateTime",
    dir: int = 1,
    search: SearchRequest = None,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    search.defaultObjectId.append(
        FieldObjectIdRequest(field="companyId", key=ObjectId(current_user.companyId))
    )
    search.defaultBool.append(FieldBoolRequest(field="isDelete", key=False))
    skip = page * size
    criteria = CreateCriteria(search)
    datas_cursor = dbase.find(criteria).skip(skip).limit(size).sort(sort, dir)
    datas = await datas_cursor.to_list(length=size)
    totalElements = await dbase.count_documents(criteria)
    totalPages = math.ceil(totalElements / size)
    reply = UserPage(  #
        content=datas,
        size=size,
        page=page,
        totalElements=totalElements,
        totalPages=totalPages,
        sortDirection=dir,
    )
    return reply


@router_admin.get("/get_admin_by_user_id", response_model=UserBasic)
async def get_admin_by_user_id(
    userId: ObjectIdStr,
    current_user: JwtToken = Security(get_current_user, scopes=["superadmin", "*"]),
):
    return await GetAdminOr404(userId)


@router_admin.put("/admin/{userId}", response_model=UserBasic)
async def update_admin(
    userId: str,
    comp: UserInput,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    return await UpdateAdmin(userId, comp.dict())


@router_admin.delete("/admin/{userId}")
async def delete_admin(
    userId: str,
    current_user: JwtToken = Security(
        get_current_user, scopes=["superadmin,admin", "*"]
    ),
):
    await GetAdminOr404(userId)
    dbase = MGDB.user_admin
    try:
        await dbase.update_one(
            {"userId": ObjectId(userId)}, {"$set": {"isDelete": True}}
        )
    except Exception as e:
        print("error: " + str(e))
        raise HTTPException(status_code=500, detail="Kesalahan internal server")
    return "Admin dengan userId " + userId + " telah dihapus"
